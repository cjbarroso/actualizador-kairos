#!/bin/bash
# ENV:
# - WEBHOOK_URL
# - CUIT_SIDISA
# - PASS_SIDISA

# EN que carpeta guardar el archivo de precios, toma nombre PRECIOS-<fecha actualizacion>.ZIP
BASE_DESCARGA_PRECIOS=/tmp
##################################################################3
PUP=~/bin/pup
TIMESTAMP=$(date +%Y%m%d%H%M%S)
ULTIMA_ACT=/tmp/ultimo_quincenal.${TIMESTAMP}.data
REFERENCIA=/tmp/ultimo-kairos.data
RUTA_BASE=http://www.sidisaonline.com.ar
LOG_FILE=/tmp/wget.${TIMESTAMP}.log
LOG_DESCARGA=/tmp/actualizacion-descargada.${TIMESTAMP}.log
COOKIES=/tmp/cookies.txt
OUTPUT_HTML=/tmp/completo.html
OUTPUT_JSON=/tmp/completo.json
LOGIN_URL=${RUTA_BASE}/log.php
PRECIOS_URL=${RUTA_BASE}/index.php\?p\=precios

# Logueo y obtengo cookies
wget -o ${LOG_FILE} --save-cookies ${COOKIES} --keep-session-cookies \
  --post-data "cuit=${CUIT_SIDISA}&pass=${PASS_SIDISA}"\
  --delete-after ${LOGIN_URL}

#  OBtengo la pagina de precios
wget -o ${LOG_FILE} --load-cookies=${COOKIES} \
  --output-document ${OUTPUT_HTML} ${PRECIOS_URL}

# Convierto a JSON
cat ${OUTPUT_HTML} | ${PUP} 'json{}' > ${OUTPUT_JSON}

# Extraigo el nombre de la ultima actualizacion quincenal
jq 'first(.[0].children[0].children[1].children[0].children[7].children[0].children[] | select(.children[0].children[0].text=="Precios - Servicio quincenal") | .children[2].children[].href)' ${OUTPUT_JSON} > ${ULTIMA_ACT}

# si es diferente al ultimo guardado, lo descargo y aviso       

if ! diff -q ${REFERENCIA} ${ULTIMA_ACT} 2>&1 >/dev/null; then
  # Son diferentes "Diferentes, descargo"
  # Obtengo el nombre del archivo
  MIRUTA=$(cat ${ULTIMA_ACT} | sed 's/^...//' | sed 's/.$//')
  # Armo la URL para descargar
  URL=${RUTA_BASE}/${MIRUTA}

  # obtengo la fecha en que fue publicado
  FECHARCH=$(echo $MIRUTA | cut -d/ -f4)

  # Obtengo el archivo y lo guardo en el servidor
  wget -o ${LOG_DESCARGA} -O ${BASE_DESCARGA_PRECIOS}/PRECIOS-${FECHARCH}.ZIP \
    --load-cookies=${COOKIES} "${URL}"

  # Lo marco como ultima version
  cp ${ULTIMA_ACT} ${REFERENCIA} 

  # Notifico en el canal de slack
  curl -X POST -H 'Content-type: application/json' \
    --data "{'text': \"Nueva actualización disponible del dia ${FECHARCH}\" }" ${SLACK_WEBHOOK}
#else
  # No hago nada por ahora

fi
